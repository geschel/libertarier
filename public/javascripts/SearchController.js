/**
 * Created by Peter_Geschel on 22.10.2015.
 */

"use strict";

var app = angular.module('searchApp', []);

app.controller('SearchCtrl', function($scope, $location, $http, $sce) {

    $scope.hitcount = '-';

    $scope.searchButtonClicked = function(){

        var txt = $scope.searchtext

        //execute only if text not empty
        if ( txt ) {
            var loadinggif = window.document.getElementById('loadingGif');
            loadinggif.style.display = '';
            $scope.hitcount = '';

            //prepare callbacks
            var successCallback = function(data){
                var j = angular.fromJson(data);

                //update display model
                loadinggif.style.display = 'none';
                document.getElementById("thesearchbutton").disabled = true;
                updateModel(j);
                $scope.searchtext = '';
            };

            var errorCallback = function(data){

                var j = angular.fromJson(data);
                alert('errorCallback:'+j);
                loadinggif.style.display = 'none';
                $scope.hitcount = '-';
                $scope.searchtext = '';
            };

            $http.get('/users/getDummyData').then(successCallback, errorCallback);
            //$http.get('http://libertarier.de/api/v1/search?term=staat').then(successCallback, errorCallback);
            //$http.get('http://localhost:9210/api/v1/search', {params: {term: $scope.searchtext}}).then(successCallback, errorCallback);
            //$http.get('http://libertarier.de/api/v1/search', {params: {term: $scope.searchtext}}).then(successCallback, errorCallback);

        } else {
            return;
        }

    };

    var updateModel = function(j) {
        $scope.hitcount = j.data.hits;
        $scope.chapters = j.data.chapters;
        for(var i=0; i<$scope.chapters.length; i++) {
            var currentChapter = $scope.chapters[i];
            currentChapter.hit = i;
            currentChapter.displaytitle = currentChapter.book_title+', '+currentChapter.author+' - Kapitel '+ currentChapter.title;
            for(var j=0; j<currentChapter.highlights.length; j++) {
                currentChapter.highlights[j].text = $sce.trustAsHtml(currentChapter.highlights[j].text);
                currentChapter.highlighttext = currentChapter.highlighttext+currentChapter.highlights[j].text+' ';
            }
            currentChapter.displaytext = $sce.trustAsHtml(currentChapter.text);
        }
    }

    $scope.openChapterClicked = function(hitnumber) {

        for(var i=0; i<$scope.chapters.length; i++) {

            var openButton = window.document.getElementById('open_button_'+i);
            var closeButton = window.document.getElementById('close_button_'+i);
            var detailTextDiv = window.document.getElementById('detailed_text_div_'+i);

            if(i === hitnumber) {
                openButton.style.display = 'none';
                closeButton.style.display = '';
                detailTextDiv.style.display = '';
            }
            else {
                openButton.style.display = '';
                closeButton.style.display = 'none';
                detailTextDiv.style.display = 'none';
            }
        }
    }

    $scope.closeChapterClicked = function(hitnumber) {

        for(var i=0; i<$scope.chapters.length; i++) {
            var openButton = window.document.getElementById('open_button_'+i);
            var closeButton = window.document.getElementById('close_button_'+i);
            var detailTextDiv = window.document.getElementById('detailed_text_div_'+i);

            if(i === hitnumber) {

                openButton.style.display = '';
                closeButton.style.display = 'none';
                detailTextDiv.style.display = 'none';
            }
        }
    }

    $scope.searchInputChanged = function() {

        var button = document.getElementById("thesearchbutton");

        if($scope.searchtext) {
            button.disabled = false;
        }
        else {
            button.disabled = true;
        }
    }

    $scope.handleJumptoClicked = function(anchor, chapternumber) {
        console.log('anchor = '+anchor+' chapter number = '+chapternumber);

        $scope.openChapterClicked(chapternumber);

        var detailsContainer = window.document.getElementById('detailed_text_div_'+chapternumber);
        var textTarget = window.document.getElementById(''+anchor);

        textTarget.style.backgroundColor = "#FFFF00";

        //var pos = getScrollTargetPosition(textTarget, detailsContainer, 0);
        var posOuter = detailsContainer.offsetTop;
        var posInner = textTarget.offsetTop;

        console.log('posInner = '+posInner+' posOuter = '+posOuter);
        window.scrollTo(0, posOuter-100);
        detailsContainer.scrollTop += (posInner - posOuter);
    }

});
