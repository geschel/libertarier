/**
 * Created by Peter_Geschel on 21.10.2015.
 */
(function () {
    "use strict";

    var semver = require("semver");
    var jshint = require("simplebuild-jshint");
    desc("default build");
    task("default", [ "version", "lint" ], function () {
        console.log("\n\nBUILD OK");

        console.log("\n\nSTARTING DEBUG SERVER...");
        jake.exec("SET DEBUG=:* & npm start", {interactive: true}, complete);
    });

    desc("checking node version");
    task("version", function () {
        console.log("checking node version: .");

        var packageJson = require("./package.json");
        var expectedVersion = packageJson.engines.node;

        var actualVersion = process.version;
        if(semver.neq(actualVersion, expectedVersion)) {
            fail("node version does not match, EXPECTED = ["+expectedVersion+"] but ACTUAL = ["+actualVersion+"]");
        }
        console.log(process.version);
    });

    desc("linting java script code");
    task("lint", function () {
        process.stdout.write("linting java script: ");
        jshint.checkFiles({
            files: "jakefile.js",
            options: {},
            globals: {}
        }, complete, fail);

        //jake.exec("node node_modules/jshint/bin/jshint jakefile.js", {interactive: true}, complete);
    }, {async: true});

}());