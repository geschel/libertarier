#!/bin/sh

[ ! -f node_modules/.bin/jake ] && echo "Building npm modules:" && npm rebuild

[ ! -f app/node_modules ] && echo "Installing npm modules:" && cd app && npm install

node_modules/.bin/jake $*